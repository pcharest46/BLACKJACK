#include <iostream> // INPUT OUTPUT afin d'avoir les cin et cout.
#include <string>
#include <cstdlib> // random number generation

#include <iomanip> // permet d'utiliser les setw.
using namespace std;

void Deck(string, string[13][4], string[52]);
void ValeurDesMains(string[13][4], string[5][4], string, int[5][4], int[1]);
void TourDeTable(string, string[52], string[13][4], string[5][4], int[5][4], int[1]);
void PrintTourDeTable(string[5][4], int[5][4], string[4], int[1]);
void Reset(string[5][4], int[5][4], string[4], bool[4]);

int main()
{
	char match = 'o';
	const int taille = 13;
	const int grandeur = 4;
	int argent[4] = { 100,100,100,100 };
	string NomValue[taille][grandeur] =
	{ { "AC","AK","AT","AP" },
	{ "2C","2K","2T","2P" },
	{ "3C","3K","3T","3P" },
	{ "4C","4K","4T","4P" },
	{ "5C","5K","5T","5P" },
	{ "6C","6K","6T","6P" },
	{ "7C","7K","7T","7P" },
	{ "8C","8K","8T","8P" },
	{ "9C","9K","9T","9P" },
	{ "10C","10K","10T","10P" },
	{ "VC","VK","VT","VP" },
	{ "DC","DK","DT","DP" },
	{ "RC","RK","RT","RP" } };

	string main[5][4] = { "0" };
	int valmain[5][4] =
	{ { 0,0,0,0 },
	{ 0,0,0,0 },
	{ 0,0,0,0 },
	{ 0,0,0,0 },
	{ 0,0,0,0 } };
	string carte;
	int val = 0;
	double valtotal = 0;
	char continu = 'o';

	int count = 0;
	string deck[52];
	string message[4] = { "0","0","0","0" };
	bool winner[4] = { false,false,false,false };
	int nbWinner = 0;
	int resetCpt[1] = { 0 };
	int max = { 0 };

	setlocale(LC_ALL, "");
	srand(time(NULL));



	/// �cran de pr�sentation
	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl;
	cout << "\t\t#########################################" << endl;
	cout << "\t\t#                GREYJACK               #" << endl;
	cout << "\t\t#########################################" << endl;
	system("pause > nul");
	system("CLS");
	cout << "\t\t*****Bienvenue au Grey Jack*****" << endl << endl;

	/// D�but de la partie. On initialise le tout � z�ro. Pr�pare le deck et on est pr�t � partir. Loop en place pour repartir de cette
	/// position lorsque requis.
	do {
		resetCpt[0] = 0;
		count = 0;
		continu = 'o';
		Reset(main, valmain, message, winner);
		Deck(carte, NomValue, deck);

		/// Premier tour ou l'on affiche les 2 oremi�res lignes avec une ligne de cartes cach�s pour les autres joueurs.
		cout << "Vous" << setw(20) << "Joueur 2" << setw(20) << "Joueur 3" << setw(20) << "Joueur 4" << endl;
		TourDeTable(carte, deck, NomValue, main, valmain, resetCpt);
		ValeurDesMains(NomValue, main, carte, valmain, resetCpt);
		resetCpt[0] = 1;
		/// Boucle en place pour l'indication de l'utilisateur de continuer � piger des cartes.
		do {


			TourDeTable(carte, deck, NomValue, main, valmain, resetCpt);
			ValeurDesMains(NomValue, main, carte, valmain, resetCpt);

			PrintTourDeTable(main, valmain, message, resetCpt);
			resetCpt[0] = 2;
			cout << endl;
			/// Conditions pour si on sort imm�diatement de cette boucle (BJ, OUT ou on est rendu � 3 tours de cartes demand�s.
			if (message[0] == "*OUT*")
			{
				continu = 'n';
			}

			// Si le message est BJ pour n'importe quel des 4 joueurs, on ne continue PAS.
			for (int i = 0; i < 4; i++)
			{
				if (message[i] == "*BJ*")
				{
					winner[i] = true;
					continu = 'n';
				}
			}

			if (count == 3)
				continu = 'n';
			else if (continu != 'n')
			{
				cout << "D�sirez-vous une autre carte?(o/n)";
				cin >> continu;
			}
			count += 1;
		} while ((continu == 'o') || (continu == 'O'));

		/// Une fois sorti de la boucle, on v�rifie, les valeurs totales et on les pr�sente sur l'interface.
		valtotal = 0;

		for (int joueur = 0; joueur < 4; joueur++)
		{
			for (int i = 0; i < 5; i++)
			{
				valtotal += valmain[i][joueur];
				if (valtotal > 21)
					valtotal = -1;
			}
			if (joueur == 0)
			{
				cout << "La valeur de votre main est====> " << valtotal << endl;

			}

			else
			{
				cout << "La valeur de la main du joueur " << joueur + 1 << " est====> " << valtotal << " et sa carte cach�e est " << main[0][joueur] << endl;

			}

			/// On v�rifie le nombre de gagnant pour la manche. On divise le lot par le nombre de gagnants.
			if (max < valtotal)
			{
				max = valtotal;
			}
			valtotal = 0;
		}
		nbWinner = 0;
		for (int joueur = 0; joueur < 4; joueur++)
		{
			for (int i = 0; i < 5; i++)
			{
				valtotal += valmain[i][joueur];
				if (valtotal > 21)
					valtotal = -1;
			}
			if (valtotal == max)
			{
				winner[joueur] = true;
				nbWinner++;
			}
			valtotal = 0;
		}
		max = 0;
		if (nbWinner > 0)
		{
			for (int i = 0; i < 4; i++)
			{
				argent[i] -= 10;
				if (winner[i] == true)
				{
					argent[i] += 40 / nbWinner;
				}
			}
		}
		/// On pr�sente les sommes de chaque joueur.
		cout << argent[0] << "$" << setw(20) << argent[1] << "$" << setw(20) << argent[2] << "$" << setw(20) << argent[3] << "$" << endl;
		cout << "D�sirez-vous jouer une autre manche (o) ou mettre fin � la partie? (n)    (o/n)";
		cin >> match;
		/// On v�rifie si l'utilisateur souhaite poursuivre la partie.


		system("CLS");

	} while (match == 'o');

	system("pause");
	return 0;
}

void Deck(string carte, string NomValue[13][4], string deck[52])
{
	//Preparer le paquet de carte
	int i = 0;

	for (int x = 0; x < 13; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			carte = NomValue[x][y];
			deck[i++] = carte;
		}
	}
	return;
}

void ValeurDesMains(string NomValue[13][4], string main[5][4], string carte, int valmain[5][4], int resetCpt[1])
{

	static int cnt = 0;
	int val = 0;
	int nbAs[4] = { 0,0,0,0 };
	double valtotal = 0;

	if (resetCpt[0] == 0)
	{
		cnt = 0;
	}

	//Pour chaque joueur, identifier la carte (i,j) puis lui attribuer une valeur.
	for (int joueur = 0; joueur < 4; joueur++)
	{
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				carte = main[cnt][joueur];
				if (carte == "0")
				{
					val = 0;
				}
				if (NomValue[i][j] == carte)
				{
					for (int k = 0; k < 5; k++)
					{
						valtotal += valmain[k][joueur];
					}
					if (valtotal > 21)
						valtotal = -1;

					//valeur pour A depend du score actuel du joueur
					if (i == 0)
					{
						nbAs[joueur] += 1;
						///Verifier si la valeur des cartes est plus que 10.
						if (valtotal > 10)
						{
							val = 1;
						}
						else
						{
							val = 11;
						}
					}
					//valeur de 2 a 10
					else if (i > 0 && i < 10)
						val = i + 1;
					else
					{
						//valeur pour V,D,R
						val = 10;
						// Entrer la valeur de la carte dans les valeur attribu�es aux carte.

					}
				}
				valmain[cnt][joueur] = val;
			}
		}
		valtotal = 0;

		for (int k = 0; k < 5; k++)
		{
			valtotal += valmain[k][joueur];
		}
		if (valtotal > 21)
			valtotal = -1;
		//Extension qui devrait permettre de changer un As pour la valeur de 1 si plus que 21 mais
		//ne fonctionne pas correctement.
		if (valtotal == -1 && nbAs[joueur] > 0)
		{
			for (int k = 0; k < 5; k++)
			{

				if (valmain[k][joueur] == 11)
				{
					valmain[k][joueur] -= 10;
					valtotal = 0;
					for (int k = 0; k < 5; k++)
					{
						valtotal += valmain[k][joueur];
						if (valtotal > 21)
							valtotal = -1;
					}
				}
			}
		}

		//Verification de la valeur totale de la main avec la nouvelle carte ajoutee
		for (int k = 0; k < 5; k++)
		{
			valtotal += valmain[k][joueur];
			if (valtotal > 21)
				valtotal = -1;
		}


		valtotal = 0;
	}
	cnt++;
	return;
}







void TourDeTable(string carte, string deck[52], string NomValue[13][4], string main[5][4], int valmain[5][4], int resetCpt[1])
{
	static int cnt = 0;
	int valtotal = 0;

	if (resetCpt[0] == 0)
	{
		cnt = 0;
	}

	//Distribution de cartes a chaque joueur. 
	for (int joueur = 0; joueur < 4; joueur++)
	{
		do {
			carte = deck[rand() % 52];
		} while (carte == "0");
		//S'assurer qu'une carte n'est tirer qu'une seule fois. Mettre la carte � z�ro une fois tir�.
		//Si un z�ro est tir�, repiger une autre carte.
		for (int i = 0; i < 52; i++)
		{
			if (deck[i] == carte)
			{
				deck[i] = "0";
			}
		}


		//Ajouter la carte � la main.
		main[cnt][joueur] = carte;

		for (int joueur = 1; joueur < 4; joueur++)
		{
			for (int k = 0; k < 5; k++)
			{
				valtotal += valmain[k][joueur];
			}
			if (valtotal > 16)
				main[cnt][joueur] = "0";
			valtotal = 0;
		}

	}
	cnt++;
	return;
}

void PrintTourDeTable(string main[5][4], int valmain[5][4], string message[4], int resetCpt[1])
{
	static int cnt = 0;
	int joueur = 0;
	int valtotal = 0;
	/// Ce compteur est particuli�rement utilise lorsqu'on recommence une partie de nouveau. Sinon le compteur statique ne cesserait
	/// d'augmenter.
	if (resetCpt[0] == 1)
	{
		cnt = 0;
	}
	/// Lors du premier tour.
	if (cnt == 0)
	{
		cout << main[cnt][joueur] << setw(20) << "**" << setw(20) << "**" << setw(20) << "**" << endl;
		cout << main[++cnt][joueur];
		for (int i = 0; i < 3; i++)
		{
			cout << setw(20) << main[cnt][++joueur];
		}
		cout << endl;

		// Verifications pour un BJ ou un OUT.
		for (int joueur = 0; joueur < 4; joueur++)
		{
			for (int k = 0; k < 5; k++)
			{
				valtotal += valmain[k][joueur];
			}

			if (valtotal == 21)
			{
				message[joueur] = "*BJ*";
			}
			else if (valtotal > 21)
				message[joueur] = "*OUT*";
			else
			{
				message[joueur] = "  ";
			}
			valtotal = 0;
		}
		/// Afficher les messages sous chaque joueur.
		cout << message[0] << setw(20) << message[1] << setw(20) << message[2] << setw(20) << message[3] << endl;
	}
	else
	{
		joueur = 0;
		cout << main[++cnt][joueur];
		for (int joueur = 1; joueur < 4; joueur++)
		{
			if (main[cnt][joueur] == "0")
				cout << setw(20) << "  ";
			else
				cout << setw(20) << main[cnt][joueur];
		}
		cout << endl;


		for (int joueur = 0; joueur < 4; joueur++)
		{
			for (int k = 0; k < 5; k++)
			{
				valtotal += valmain[k][joueur];
			}
			if (valtotal > 21)
				message[joueur] = "*OUT*";
			else
			{
				message[joueur] = "  ";
			}
			valtotal = 0;
		}
		cout << message[0] << setw(20) << message[1] << setw(20) << message[2] << setw(20) << message[3] << endl;
	}

	return;
}
void Reset(string main[5][4], int valmain[5][4], string message[4], bool winner[4])
{
	for (int i = 0; i<5; i++)
		for (int j = 0; j < 4; j++)
		{
			{
				main[i][j] = "0";
				valmain[i][j] = 0;
			}
		}

	for (int i = 0; i < 4; i++)
	{
		message[i] = "0";
		winner[i] = false;
	}

	return;
}